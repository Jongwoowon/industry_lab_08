package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        FileReader fR = null;
        int num;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        try {
            fR = new FileReader("input2.txt");

            // finding the total length of characters.
            while (fR.ready()) {
                num = fR.read();
                if (num != -1) {
                    total++;
                    if (num == 69 || num == 101) {
                        numE++;
                    }
                }
            }

        } catch (IOException e) {
            System.out.println("IO Problem");
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        String line = null;

        try (BufferedReader reader = new BufferedReader(new FileReader("input2.txt"))) {

            while ((line = reader.readLine()) != null) {
                total += line.length();
                line = line.toLowerCase();
                numE += (line.length() - line.replaceAll("e", "").length());
            }

        } catch (IOException e) {

        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.


        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
