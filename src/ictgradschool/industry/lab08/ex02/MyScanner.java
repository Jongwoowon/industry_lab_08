package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.FileReader;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        try (Scanner k = new Scanner(new FileReader(fileName + ".txt"))){
            while (k.hasNextLine()){
                System.out.println(k.nextLine());
            }

        }
        catch (IOException e){
        }
        catch (NoSuchElementException e){
        }

    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
